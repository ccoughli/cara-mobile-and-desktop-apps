---
  kind: "Template"
  apiVersion: "v1"
  metadata:
    name: "cara-application"
    annotations:
      description: "CARA application OpenShift template."
      tags: "cara-application"
  labels:
    template: "cara-application"
  objects:
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        name: auth-service
        labels:
          app: auth-service
      spec:
        replicas: 1
        template:
          metadata:
            labels:
              app: auth-service
              deploymentconfig: auth-service
          spec:
            containers:
              - name: auth-service
                image: '${PROJECT_NAME}/auth-service'
                ports:
                  - containerPort: 8080
                    protocol: TCP
                envFrom:
                  - configMapRef:
                      name: auth-service
                  - secretRef:
                      name: auth-service-secrets
                imagePullPolicy: Always
                resources: {}
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
            dnsPolicy: ClusterFirst
            restartPolicy: Always
            schedulerName: default-scheduler
            securityContext: { }
            terminationGracePeriodSeconds: 30
        strategy:
          activeDeadlineSeconds: 21600
          resources: { }
          rollingParams:
            intervalSeconds: 1
            maxSurge: 25%
            maxUnavailable: 25%
            timeoutSeconds: 600
            updatePeriodSeconds: 1
          type: Rolling
        test: false
        selector:
          deploymentconfig: auth-service
        triggers:
          - type: ConfigChange
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
                - auth-service
              from:
                kind: ImageStreamTag
                name: 'auth-service:latest'
                namespace: ${PROJECT_NAME}
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        name: cara-app
      spec:
        replicas: 1
        template:
          metadata:
            labels:
              app: cara-app
          spec:
            containers:
              - name: cara-app
                env:
                - name: APP_NAME
                  value: cara-voila
                image: '${PROJECT_NAME}/cara-app'
                ports:
                  - containerPort: 8080
                    protocol: TCP
                imagePullPolicy: Always
                resources: {}
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
            dnsPolicy: ClusterFirst
            restartPolicy: Always
            schedulerName: default-scheduler
            securityContext: { }
            terminationGracePeriodSeconds: 30
        strategy:
          activeDeadlineSeconds: 21600
          resources: { }
          rollingParams:
            intervalSeconds: 1
            maxSurge: 25%
            maxUnavailable: 25%
            timeoutSeconds: 600
            updatePeriodSeconds: 1
          type: Rolling
        test: false
        selector:
          app: cara-app
        triggers:
          - type: ConfigChange
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
                - cara-app
              from:
                kind: ImageStreamTag
                name: 'cara-app:latest'
                namespace: ${PROJECT_NAME}
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        name: cara-router
      spec:
        replicas: 1
        template:
          metadata:
            labels:
              app: cara-router
          spec:
            containers:
              - name: cara-router
                image: '${PROJECT_NAME}/cara-router'
                ports:
                  - containerPort: 8080
                    protocol: TCP
                  - containerPort: 8443
                    protocol: TCP
                imagePullPolicy: Always
                resources: {}
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
            dnsPolicy: ClusterFirst
            restartPolicy: Always
            schedulerName: default-scheduler
            securityContext: { }
            terminationGracePeriodSeconds: 30
        strategy:
          activeDeadlineSeconds: 21600
          resources: { }
          rollingParams:
            intervalSeconds: 1
            maxSurge: 25%
            maxUnavailable: 25%
            timeoutSeconds: 600
            updatePeriodSeconds: 1
          type: Rolling
        test: false
        selector:
          app: cara-router
        triggers:
          - type: ConfigChange
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
                - cara-router
              from:
                kind: ImageStreamTag
                name: 'cara-router:latest'
                namespace: ${PROJECT_NAME}
          - type: ConfigChange
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        name: cara-webservice
      spec:
        replicas: 1
        template:
          metadata:
            labels:
              app: cara-webservice
          spec:
            containers:
              - name: cara-webservice
                env:
                - name: COOKIE_SECRET
                  valueFrom:
                    secretKeyRef:
                      key: COOKIE_SECRET
                      name: auth-service-secrets
                - name: APP_NAME
                  value: cara-webservice
                - name: CARA_CALCULATOR_PREFIX
                  value: /calculator-cern
                - name: CARA_THEME
                  value: cara/apps/calculator/themes/cern
                image: '${PROJECT_NAME}/cara-webservice'
                ports:
                  - containerPort: 8080
                    protocol: TCP
                imagePullPolicy: Always
                resources: {}
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
            dnsPolicy: ClusterFirst
            restartPolicy: Always
            schedulerName: default-scheduler
            securityContext: { }
            terminationGracePeriodSeconds: 30
        strategy:
          activeDeadlineSeconds: 21600
          resources: { }
          rollingParams:
            intervalSeconds: 1
            maxSurge: 25%
            maxUnavailable: 25%
            timeoutSeconds: 600
            updatePeriodSeconds: 1
          type: Rolling
        test: false
        selector:
          app: cara-webservice
        triggers:
          - type: ConfigChange
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
                - cara-webservice
              from:
                kind: ImageStreamTag
                name: 'cara-webservice:latest'
                namespace: ${PROJECT_NAME}
          - type: ConfigChange
    -
      apiVersion: v1
      kind: DeploymentConfig
      metadata:
        name: cara-calculator-open
      spec:
        replicas: 1
        template:
          metadata:
            labels:
              app: cara-calculator-open
          spec:
            containers:
              - name: cara-calculator-open
                env:
                - name: APP_NAME
                  value: cara-webservice
                - name: CARA_CALCULATOR_PREFIX
                  value: /calculator-open
                image: '${PROJECT_NAME}/cara-webservice'
                ports:
                  - containerPort: 8080
                    protocol: TCP
                imagePullPolicy: Always
                resources: {}
                terminationMessagePath: /dev/termination-log
                terminationMessagePolicy: File
            dnsPolicy: ClusterFirst
            restartPolicy: Always
            schedulerName: default-scheduler
            securityContext: { }
            terminationGracePeriodSeconds: 30
        strategy:
          activeDeadlineSeconds: 21600
          resources: { }
          rollingParams:
            intervalSeconds: 1
            maxSurge: 25%
            maxUnavailable: 25%
            timeoutSeconds: 600
            updatePeriodSeconds: 1
          type: Rolling
        test: false
        selector:
          app: cara-calculator-open
        triggers:
          - type: ConfigChange
          - type: ImageChange
            imageChangeParams:
              automatic: true
              containerNames:
                - cara-calculator-open
              from:
                kind: ImageStreamTag
                name: 'cara-webservice:latest'
                namespace: ${PROJECT_NAME}
          - type: ConfigChange

  parameters:
    - name: PROJECT_NAME
      description: The name of this project, e.g. test-cara
      required: true
